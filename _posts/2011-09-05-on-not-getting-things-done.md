---
layout: post
title: "On not getting things done"
date: 2011-09-05
comments: false
categories: []
---

> Sidenote from 2021: original URL for this post was http://zombie-storage.com/?p=15

I’m a fairly active productive procrastinator. I’m often easily distracted but when I focus I can block out almost any imaginable distraction. This dichotomy between distraction and work is the bane of many people but I find it particularly useful because when I’m productive, I’m super productive. While working, I am focused on the task or on related tasks like improving design, using a better software development strategy or learning a technology, programming language or style I may need in the near future.

I’m working on a progressively growing project. The codebase is presently at about 10k lines of very rapidly developed code. I’m having to wear several hats and I’m presently the only soul on the project. There is a lot to learn about managing a project this size and so far most of my method has been emulating great project managers I’ve worked with and in drawing from my experience with large writing projects. Maintenance writing is much the same as maintenance coding, you see similar craziness and learn a great deal more when you are forced to work with Past You (Past You being the guy who came before you and wrote the original version that has those shitty issues you are now fixing). However, I am spending most of my time at the bleeding edge doing 95% development. If it wasn’t test first I would be far more concerned about that I am producing at that rate. Development is ongoing, direction is agile and scope is growing steadily.

Meanwhile in my personal side projects I am teaching myself how to sculpt, Korean, starting to build a 3D printer, some insanity with javascript and CSS to create website degradation without security risks and playing a lot of games. I need to start finishing some of these and release them. There is a satisfaction you get from actually finishing something you created which when I am not getting it I start to get a hunger for that must be satisfied to hold back apathy and dejection. My working memory is pretty good and I can remember several things at once for a couple of days. I am often doing a multitude of tasks at the same time as well as more mundane tasks for daily existence (cooking, buying stuff, cleaning, etc). If that wasn’t the case I’m sure I would be nothing more than a terrible hoarder or half finished projects.

Feed your hungry beast. Get distracted. Satisfy your intellectual curiosity but remember that no one wants a half finished job. It is not appropriate for housing and it is certainly not appropriate for your coding side project. Deliver something, anything, regularly. Despite the distractions finish something big at least once a week. You can always hack, fix, amend or update that whole, finished thing later. That can, and often should, be done once your first goal is met. Now go out there and meet some goals.

I’ll post whatever it was I finished on Friday.