---
layout: post
title: "And, we’re back"
date: 2011-06-29
comments: false
categories: []
---

> Sidenote from 2021: original URL for this post was http://zombie-storage.com/?p=10

Sorry, dear reader. My blog was offline for a month or so because my server wasn’t actually plugged in. Pretty slack, I know. In the mean time I’ve been working on some wicked JS/CSS for a better site as well as becoming a Rails grandmaster in my day job. There will be good things here eventually. I may even write something of interest. Who knows, I’m still just working hard and long hours so I don’t get a lot of free time. Oh, and I have too many other hobbies and commitments. Haha! Life!

