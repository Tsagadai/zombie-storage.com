---
layout: post
title: "Port 25 will be blocked for South Korea from December"
date: 2011-11-08
comments: false
permalink: https://zombie-storage.com/?p=95
categories: []
---

> Sidenote from 2021: original URL for this post was http://zombie-storage.com/?p=95
> This post was also extremely popular and got over 2 million unique views in the first week it was writen in. It was featured on Hacker News, Reddit and others and taught me a lot about self-hosting wordpress on an Atom box in my apartment.
> There were also comments on this article that can be seen on archive.org <https://web.archive.org/web/20120118012352/http://www.zombie-storage.com/>

So it turns out that what I thought was just a scam message is actually true:
The South Korean government is attempting to fight spam by issuing a national decree to block port 25 and use 587 instead. Here is the decree that I got sent in an email:
“방송통신위원회(<http://www.kcc.go.kr/>)와 한국인터넷진흥원(<http://www.kisa.or.kr/>)이 공동으로 진행하는 정부시책 스팸 줄이기의 일환으로 ‘블럭25 프로젝트’ 가 12월부터 본격적으로 시행됩니다.”
Rough translation:
The Korean Communications Commission (kcc.go.kr, the telco regulator) and the Korean Information Security Agency (kisa.go.kr, the censors and net cops) have a joint policy to reduce spam. A national block of port 25 will be instituted in December.
기존 발송포트인 25번을 정부에서 일괄적으로 막기 때문에 아웃룩을 사용하시는 분들은 메일 발송이 되지 않습니다.

Rough translation:
Something about MS Outlook users failing so email will be batched.

I’m trying to find the actual legislation so I can comply with it but no one seems to know where it is is. KISA has a little website that talks about Outlook but doesn’t say anything of value for legitimate developers. You can find their “helpful” website here:
<http://www.block25.or.kr>

What should I do?

If you have a server in South Korea you probably should change your app to use port 587 for outbound traffic and port 465 for SMTP over SSL. If you are outside South Korea be aware that all traffic sent over port 25 will probably be intercepted from December onwards.



Side discussion:
Do these government agencies actually think blocking port 25 will reduce spam? Most of the issues stem from open relays (yes, I’ve noticed several on my IP block) or insecure servers. Botnets are a problem but using another port doesn’t fix that. From what I can tell they are also recommending people break port 587 by accepting both SMTPS and SMTP connections. Further they recommend 465 for SMTPS just to break the spec more.

The Korean government has long had a system of deep packet inspection so the technical implementation of a nationwide firewall over a port isn’t new or unprecedented. The steps may prevent some spam but really, I doubt it will amount to anything substantial. They can’t block all email and the system would probably fall over with IP forging techniques.
Update: I’ve asked a few people and it seems to be a joint initiative by the two departments and not actually a law. Although it does have penalties and apparently you get emailed warnings the first few times.



Links and further reading:

Here is a post that is the closest you can get to an official requirements list:
<http://www.block25.or.kr/ask/faq_read.jsp?idx=65&lmenu=13>

Almost all the news seems to be copied and blogspammed comes from these pages [in Korean]:

<http://www.block25.or.kr/email/spam.jsp?lmenu=01>

<http://www.block25.or.kr/email/purpose.jsp?lmenu=02>

<http://www.block25.or.kr/email/effect.jsp?lmenu=03>