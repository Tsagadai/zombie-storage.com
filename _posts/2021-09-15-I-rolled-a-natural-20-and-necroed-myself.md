---
layout: post
title: "I rolled a natural 20 and necroed myself"
date: 2021-09-15
comments: false
categories: []
---

Oh, boy has it been quiet here. I have used [wayback machine](archive.org) to pull a bunch of older content into the repo so that it can be shared with you beautiful people. There was no real reason to do that except I get annoyed by broken links. Really annoyed. It seems every time I reach back into my bookmarks for something the link is dead. I intend to resolve that soon by doing a scrape of everything I have ever bookmarked and storing it for the apocalypse/link death. To try and fix my own sins in that problem I've pull back old content I wrote. I didn't get everything as some of it is just gone into the great beyond, sadly, I've lost the early source database for this blog. 

I've come out of a bit of a rut. You probably know it well. That whole virus thing, lockdowns, etc. I am keen to get back into it all and I am somewhat horrified at the amount I used to do. It was a lot. I'm still busy now but more busy instead of closing, busy instead of making. I need to make more.

This was fun though, necroing all my old posts in a few places back to life. Fits the theme.

Please forgive me for the utter abomination of CSS that exists here presently. It hasn't been updated in a long, long time and worse than that things have been merged together to make it even worse.