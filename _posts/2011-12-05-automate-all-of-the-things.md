---
layout: post
title: "How I automated a repetitive task, or Git hooks and why you should use them"
date: 2011-12-05
comments: false
categories: [Hacks, Linux, Rails, gemfile, git, hacks, rails, rubygems, sed]
---

> Sidenote: original URL for this post was http://zombie-storage.com/?p=109

Say you have a repetitive task and you use git. You want to make a minor change every time before you commit.

My specific problem is that I have a Windows development machine and a Linux cluster. Foolishly, some Ruby Gems have different package names for Windows and Linux, which is just annoying and breaks Capistrano’s nice deploy command. The solution is to remove the alternate version tag “-x86-mingw32″ in my case and replace it with nothing. sed is formatted as “s/<the string or regex you are looking for/<string or regex with which to replace it>”.

Here’s my script that I added to my ./git/hooks/pre-commit file. I am changing the permissions and suppressing an error because Windows sed does something stupid and locks the file.

#!/bin/sh
# it just overrides the Gemfile.lock variable.
echo "Changing Gemfile.lock for production compatibility"
sed -i 's/-x86-mingw32//g' Gemfile.lock &>/dev/null
chmod 744 Gemfile.lock
echo 0
Now, every time I use git commit that little script will run and attempt to change the Gemfile.lock file.

This probably isn’t the best way to do it, it’s probably not even correct but it works and it saves me time.