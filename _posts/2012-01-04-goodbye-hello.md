---
layout: post
title: "Goodbye, 2011. Hello, 2012."
date: 2012-01-04
comments: false
categories: []
---

> Sidenote: original URL for this post was http://zombie-storage.com/?p=115 I don't really want to fix the mess that wordpress created.


It’s that time of year. The brief gap between Christmas spam, post-Christmas sales spam and before Valentines day and Easter spam. Where gym memberships soar and commitments are made that will be broken within mere weeks of their inception.

Well, not really for me. Christmas in Korea is largely a commercial holiday aimed at couples and New Years doesn’t have the same significance as the lunar new year is seem as the more important event. Saturation advertising is much more tolerable when you can block it out by simply not paying any attention to it. I need to actively read and listen to understand Korean so it is easy to not be drawn in by advertising (plus, I don’t watch TV).

I have felt compelled to blog though. It has been too long since I posted anything and from what my logs are telling me there are a lot of hijacking attempts and unique, regular visitors. I like amusing both people and robots so I better keep regularly writing.

So what have I been up to over these weeks of absence, you may ask. Well, it has been a cold and isolating winter. My significant other has been on holiday for the last few weeks in warmer climes. This has strangely caused me to get less done with my free time. I’ve been snowboarding a few times, I’m entirely self-taught with youtube and observation so I am surprised that I’m now a semi-passable snowboarder (considering I am from the tropics and I’ve only seen three winters with snow in my life).

That, and gaming has eaten my time. Mainly Battlefield 3 and Skyrim. I am very pleased with FXAA (fast approximate anti-aliasing, http://www.codinghorror.com/blog/2011/12/fast-approximate-anti-aliasing-fxaa.html). FXAA is probably one of the greatest settings ever, huge jump in performance and noticeable quality. If you aren’t using it, tick that damn box or force it to use that from the graphics card and disable other anti-aliasing.

I’m also getting my nerd on. I’ve been getting back into Warhammer because the gaming group here is friendly. Orctown (http://www.orctown.com) is a good hobby store and there is no sales tax here so prices usually end up lower than mail-order too.

I’m presently improving an arduino powered weather monitor I made last month. I’ll be writing it up with pictures and code for everyone out there. It’s a simple project but I had a need (to know what temperature it is) and some tools (an arduino) to figure it out on my own. I like making things and I find it very satisfying when I had a particularly difficult or unproductive afternoon to get something done at night. To achieve something. Success on a regular basis is important for maintaining your sanity and happiness. Building things and feeling your physical connection to the world around you will definitely improve your life.

Be seeing you.