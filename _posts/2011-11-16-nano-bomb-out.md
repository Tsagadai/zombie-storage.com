---
layout: post
title: "How I automated a repetitive task, or Git hooks and why you should use them"
date: 2011-11-16
comments: false
categories: [Nanowrimo]
---

> Sidenote: original URL for this post was http://zombie-storage.com/?p=103

I have pretty much given up on Nanowrimo for this year. I am just too far behind now for the amount of time I have left. I’ll probably try to fill it out into a short story and upload the 2 other chapters that aren’t here yet. I will probably also convert it into a nice .epub book with some editing, eventually because Creative Commons books definitely have some fans out there. Some of the reasons I’m giving up:

I don’t have enough time.

I thought it I just cut back on everything else I usually do in my evenings (gaming, classes, messing about on the net, TV, programming) I would have enough time. Turns out I was wrong there. I work at least 50 hours a week at time moment. Part of it is the Confucian doctrine of working long hours (because working long hours shows loyalty and is the path to perfection, if you actually believe it), part of it is my own interest in my work project, part of it is just plain peer-pressure. Whatever the case, I usually don’t get home until after 8pm and I am not functional in the mornings until after a coffee. Other tasks (keeping my apartment in order, food preparation, general hygiene, eating, and sleeping) resulted in me getting about 3 to 4 hours of writing time per day. That was enough to get about 2,000 words done but I am past that point now. Couple that with a few weekend of not writing and you see why I am so far behind.

I’m not excited enough by my story

That is such a Gen Y complaint but it is unfortunately true. I like my idea, and I mostly liked my implementation but just liking it isn’t getting me so excited I can’t sleep. I’ve written over 60,000 words before during the course of a long weekend when I was really, really excited by an idea. I can’t summon that sort of manic stamina on command. Not excited enough to sacrifice sleep for.

I’m too rational (and irrational)

I can’t beat the idea that I have already failed by this point out of my head. I would need to average almost 2,000 words a day to get over the line at the moment and I don’t have the time to write that many words in a day. I can’t fool myself into doing something. I know what is possible for myself but I also know of unknown unknowns and I am a good judge of my own performance now. I know I could not slog through the writing while working at the moment with the time and word count remaining. ‘Believe in yourself’ is crap. I know what I can do to a fairly low margin of error. That isn’t to say I couldn’t surprise myself, I probably could but the chance is too low (I’d give it 15% success). I am a risk seeking person but I do not rely on risks for success. Controlled risk. It is the difference between betting everything you have on Black and betting a months wages on Black. Both take a risk, but one is a very stupid move while the other is a 49.5% at becoming poorer or richer.

Lack of peer-pressure

I think success in Nano weighs very heavily on having peers to keep you motivated. This year I haven’t been to any write-ins because the times and places have been inopportune. Social networks don’t have the power mocking nag or gloating remark in plain text. Maybe someone should make a “dramatic reader” plug-in that converts posts to the appropriate emotion you need to hear those words read in. It is much harder without a support group.

Writing on public transport is hard here

Last year I wasn’t working so that doesn’t count but previously I wrote quite a lot while travelling. I really haven’t been doing that here in Korea. Often the buses/subways I take to and from work are full and I’m standing the entire journey. When it isn’t full I usually end up seated next to someone who takes up too much room in the tiny seats making it impossible to touchtype.



There are a lot of complaints there. I’m admitting that I really can’t write a novel in my spare time, in a single month, at the moment. Under different circumstances or stronger discipline in those first two weeks I possibly could have but now I am over the point of no return. I will finish it though as I am keen to get it out there and I still like it enough to want other to read it. Most importantly, I am going to donate $50. Mostly as a guilty submission to defeat but also to give to a good cause. You should donate too.



My next post will probably be what I learnt from Hacker News giving me an old-fashioned slashdotting earlier this month. It was a coming-of-age event for me as a network administrator (that was the first time it has ever happened to my server). I’ve had ~30,000 unique visitors this month and at it’s peak I was getting 22,000 hits per hour.



Until next time, be seeing you.