---
layout: post
title: "And we are back"
date: 2011-11-04
comments: false
categories: [Linux, rants]
---

> Sidenote from 2021: original URL for this post was http://zombie-storage.com/?p=87

My server has been down for most of the day and night after a failed update. Ubuntu decided that MySQL didn’t really warrant much testing and the update that was pushed caused multiple failures and resulted in some serious debugging. Given how tiny this server is (it fits in a small box under my switch and router) apt took most of the morning to reconfigure the failed transactions but it is back now. Enjoy the goodies that this site provides and laugh at my slip to less than 95% uptime.