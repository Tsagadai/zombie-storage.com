---
layout: post
title: "Embedding objects with Fabrication in Rails 3.1"
date: 2011-11-01
comments: false
categories: [Nanowrimo2011]
---

> Sidenote from 2021: original URL for this post was http://zombie-storage.com/?p=66

Let us all be honest, this isn’t my best work. Nano is about getting ideas out and getting something written. Nano is about enjoying writing because writing is enjoyable. Let that be a reminder that this is not perfect or a final draft. However, comments, criticisms and corrections are always welcome.


